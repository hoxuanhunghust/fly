<?php

define("DS", DIRECTORY_SEPARATOR);
define("ROOT", getcwd() . DS);
define("APP_PATH", ROOT . 'application' . DS);

define("CONFIG_PATH", "config" . DS);
define("CONTROLLER_PATH", APP_PATH . "controllers" . DS);
define("MODEL_PATH", APP_PATH . "models" . DS);
define("VIEW_PATH", APP_PATH . "views" . DS);
define("CORE_PATH", "core" . DS);
define("LIB_PATH", "library" . DS);

if($_REQUEST['user'] == "admin") define("PLATFORM", "admin");
else if($_REQUEST['user'] == "login") define("PLATFORM", "login");
else define("PLATFORM", "home");
// define("CONTROLLER", isset($_REQUEST['controller']) ? $_REQUEST['controller'] : 'Index');
define("CONTROLLER", ucfirst(PLATFORM));
define("ACTION", isset($_REQUEST['page']) ? $_REQUEST['page'] : 'index');
define("CURR_CONTROLLER_PATH", CONTROLLER_PATH . ucfirst(PLATFORM) . '.php');
define("CURR_VIEW_PATH", VIEW_PATH . PLATFORM . DS);

require CORE_PATH . "Controller.class.php";
require CORE_PATH . "Model.class.php";
require LIB_PATH . "Mysql.class.php";
require LIB_PATH . "Email.class.php";
require LIB_PATH . "Userdata.class.php";
require CURR_CONTROLLER_PATH;
require "core/Loader.php";

session_start();