# Đặt vé máy bay trực tuyến

![alt text](model.png "Sơ đồ hệ thống")

## Cách cài đặt:
1. Tải xampp: https://www.apachefriends.org/index.html
2. Bật Xampp với Apache, Mysql,..
3. Truy cập localhost:PORT/phpmyadmin -> Tạo CSDL tên: "fly" và nhập file fly.sql vào.
4. truy cập thư mục htdocs và clone code
5. Truy cập localhost:PORT/fly