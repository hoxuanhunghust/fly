<br>
<?php
$data = array();
?>
<div class="container">
    <form action="<?php echo WEB_PATH . "index.php?user=admin&page=add_flight&do=add"; ?>" method="post">
        <label>THÔNG TIN CHUNG</label>
        <div class="row">
            <div class="col-25">
                <label for="uname"><b>Mã chuyến bay</b></label>
            </div>
            <div class="col-75">
                <input type="text" placeholder="Mã chuyến bay" name="code" value="<?php echo $data['code'] ?>">
            </div>
        </div>

        <div class="row">
            <div class="col-25">
                <label for="uname"><b>Tên chuyến bay</b></label>
            </div>
            <div class="col-75">
                <input type="text" placeholder="Hà Nội - Sài Gòn" name="name" value="<?php echo $data['name'] ?>">
            </div>
        </div>

        <div class="row">
            <div class="col-25">
                <label for="uname"><b>Giờ khởi hành</b></label>
            </div>
            <div class="col-75">
                <input type="datetime-local" name="starttime" value="<?php echo $data['starttime'] ?>">
            </div>
        </div>

        <div class="row">
            <div class="col-25">
                <label for="uname"><b>Giờ hạ cánh</b></label>
            </div>
            <div class="col-75">
                <input type="datetime-local" name="endtime" value="<?php echo $data['endtime'] ?>">
            </div>
        </div>

        <div class="row">
            <div class="col-25">
                <label for="uname"><b>Giá</b></label>
            </div>
            <div class="col-75">
                <input type="text" placeholder="3000000" id="price" name="price" value="<?php echo number_format($data['price']) ?>">
            </div>
        </div>

        <br>
        <div class="row">
            <div class="col-25">
                <label for="uname"><b> </b></label>
            </div>
            <div class="col-75">
                <input type="submit" value="Thêm">
            </div>
        </div>
    </form>
</div>
<br>
<br>