<br>
<?php if (isset($page_data["success"])) if ($page_data["success"]) echo "<h2 style='color: green'>Đặt vé thành công</h2>" ?>
<?php if (isset($page_data["delete"])) if ($page_data["delete"]) echo "<h2 style='color: red'>Huỷ vé thành công</h2>" ?>
<div class="row">
    <div class="col-75">
        <a href='<?php echo WEB_PATH . "index.php?user=admin&page=add_flight"; ?>'><button> Thêm chuyến bay </button></a>
    </div>
</div>

<datalist id="flight_name_lists">
    <?php foreach ($page_data["flight_name_lists"] as $flight_name) {
        echo "<option>" . $flight_name . "</option>";
    } ?>
</datalist>

<datalist id="flight_price_lists">
    <?php foreach ($page_data["flight_price_lists"] as $flight_price) {
        echo "<option>" . $flight_price . "</option>";
    } ?>
</datalist>

<datalist id="flight_code_lists">
    <?php foreach ($page_data["flight_code_lists"] as $flight_code) {
        echo "<option>" . $flight_code . "</option>";
    } ?>
</datalist>

<div class="container">
    <form action="<?php echo WEB_PATH . "index.php?user=admin&page=manager_flight&do=search"; ?>" method="post">
        <div class="row">
            <div class="col-25">
                <label for="uname"><b>Tên chuyến bay:</b></label>
            </div>
            <div class="col-75">
                <input type="text" placeholder="Tên chuyến bay" name="name" list="flight_name_lists">
            </div>
        </div>

        <div class=" row">
            <div class="col-25">
                <label for="uname"><b>Thời gian khởi hành:</b></label>
            </div>
            <div class="col-75">
                <input type="datetime-local"  placeholder="YYYY-MM-DD" name="starttime">
            </div>
        </div>

        <div class="row">
            <div class="col-25">
                <label for="uname"><b>Thời gian hạ cánh:</b></label>
            </div>
            <div class="col-75">
                <input type="datetime-local"  placeholder="YYYY-MM-DD" name="endtime">
            </div>
        </div>

        <div class="row">
            <div class="col-25">
                <label for="uname"><b>Mã chuyến bay:</b></label></div>
            <div class="col-75">
                <input type="text" placeholder="Mã chuyến bay" name="code" list="flight_code_lists">
            </div>
        </div>

        <div class="row">
            <div class="col-25">
                <label for="uname"><b>Giá:</b></label>
            </div>
            <div class="col-75">
                <input type="text" placeholder="Mức giá" id="price" name="price" list="flight_price_lists">
            </div>
        </div>

        <br>
        <div class="row">
            <div class="col-25">
                <label for="uname"><b> </b></label>
            </div>
            <div class="col-75">
                <input type="submit" value="Tìm kiếm">
            </div>
        </div>
    </form>
</div>

<?php
if ($page_data["flight"]) {
?>

    <h1>DANH SÁCH CHUYẾN BAY</h1>
    <table style="width:100%">
        <tr>
            <th>STT</th>
            <th>Tên chuyến bay</th>
            <th>Mã</th>
            <th>Giờ khởi hành</th>
            <th>Giờ đến</th>
            <th>Giá vé</th>
            <th>Sửa</th>
            <th>Xoá</th>
        </tr>
        <?php
        $stt = 1;
        foreach ($page_data["flight"] as $key => $value) { ?>
            <tr>
                <td><?php echo $stt++; ?></td>
                <td><?php echo $value["name"]; ?></td>
                <td><?php echo $value["code"]; ?></td>
                <td><?php echo $value["starttime"]; ?></td>
                <td><?php echo $value["endtime"]; ?></td>
                <td><?php echo number_format($value["price"]); ?></td>
                <td><a href='<?php echo WEB_PATH . "index.php?user=admin&page=edit_flight&do=edit&flight_id=" . $value["flight_id"]; ?>'><button class="edit">Sửa</button></a></td>
                <td><a href='<?php echo WEB_PATH . "index.php?user=admin&page=manager_flight&do=delete&flight_id=" . $value["flight_id"]; ?>'><button class="delete">Xoá</button></a></td>
            </tr>
        <?php } ?>
    </table>
    <br>
    <br>
<?php } else echo '<h4>Không tìm thấy chuyến bay nào!</h4>'; ?>
