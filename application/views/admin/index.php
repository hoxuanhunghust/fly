<br>

<div class="row">
    <div class="col-75">
        <a href='<?php echo WEB_PATH . "index.php?user=admin&page=add_user"; ?>'><button> Thêm khách hàng </button></a>
    </div>
</div>

<h1>DANH SÁCH NGƯỜI DÙNG HỆ THỐNG</h1>
<table style="width:100%">
  <tr>
    <th>STT</th>
    <th>Họ và tên</th>
    <th>Email</th>
    <th>Xem lịch sử chuyến bay</th>
    <th>Sửa</th>
    <th>Xoá</th>
  </tr>
  <?php 
  $stt = 1;
  foreach($page_data["user_list"] as $key => $value){ ?>
  <tr>
    <td><?php echo $stt++; ?></td>
    <td><?php echo $value["name"]; ?></td>
    <td><?php echo $value["email"]; ?></td>
    <td><a href=<?php echo WEB_PATH . "index.php?user=admin&page=history_booking&user_id=".$value["user_id"]; ?>><button class="history">Xem lịch sử</button></a></td>
    <td><a href=<?php echo WEB_PATH . "index.php?user=admin&page=manager_user&do=edit&user_name=".$value["username"]; ?>><button class="edit">Sửa</button></a></td>
    <td><a href=<?php echo WEB_PATH . "index.php?user=admin&page=manager_user&do=delete&user_id=".$value["user_id"]; ?>><button class="delete">Xoá</button></a></td>
  </tr>
  <?php }?>
</table>
<br>
<br>