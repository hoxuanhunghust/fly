
<br> 
<?php if(isset($page_data["success"])) if($page_data["success"]) echo "<h4 style='color: green'>Cập nhật thông tin thành công</h4>" ?>
<?php if(isset($page_data["error"])) if($page_data["error"]) echo "<h4 style='color: red'>Mật khẩu nhập lại không trùng</h4>" ?>
<?php
foreach ($page_data["profile"] as $data) {
?>
    <div class="container">
        <form action="<?php echo WEB_PATH . "index.php?user=admin&page=edit_flight&do=update&flight_id=".$data['flight_id']; ?>" method="post">
            <label>THÔNG TIN CHUNG</label>
            <div class="row">
                <div class="col-25">
                    <label for="uname"><b>Mã chuyến bay</b></label>
                </div>
                <div class="col-75">
                    <input type="text" placeholder="Mã chuyến bay" name="code" value="<?php echo $data['code'] ?>">
                </div>
            </div>

            <div class="row">
                <div class="col-25">
                    <label for="uname"><b>Tên chuyến bay</b></label>
                </div>
                <div class="col-75">
                    <input type="text" placeholder="Hà Nội - Sài Gòn" name="name" value="<?php echo $data['name'] ?>">
                </div>
            </div>

            <div class="row">
                <div class="col-25">
                    <label for="uname"><b>Giờ khởi hành</b></label>
                </div>
                <div class="col-75">
                    <input type="datetime-local" name="starttime" value="<?php echo date('Y-m-d\TH:i', strtotime($data['starttime']));?>">
                </div>
            </div>

            <div class="row">
                <div class="col-25">
                    <label for="uname"><b>Giờ hạ cánh</b></label>
                </div>
                <div class="col-75">
                    <input type="datetime-local" name="endtime" value="<?php echo date('Y-m-d\TH:i', strtotime($data['endtime'])); ?>">
                </div>
            </div>

            <div class="row">
                <div class="col-25">
                    <label for="uname"><b>Giá</b></label>
                </div>
                <div class="col-75">
                    <input type="text" placeholder="3000000" id="price" name="price" value="<?php echo number_format($data['price']) ?>">
                </div>
            </div>

            <br>
            <div class="row">
                <div class="col-25">
                    <label for="uname"><b> </b></label>
                </div>
                <div class="col-75">
                    <input type="submit" value="Cập nhật">
                </div>
            </div>
        </form>
    </div>
    <br>
    <br>
<?php
}
?> 