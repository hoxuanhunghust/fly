<br>
<?php if (isset($page_data["username"])) $data['username'] = $page_data['username'] ?>
<?php if (isset($page_data["name"])) $data['name'] = $page_data['name'] ?>
<?php if (isset($page_data["phone"])) $data['phone'] = $page_data['phone'] ?>
<?php if (isset($page_data["email"])) $data['email'] = $page_data['email'] ?>
<?php if (isset($page_data["id_card"])) $data['id_card'] = $page_data['id_card'] ?>
<div class="container">
    <form action="<?php echo WEB_PATH . "index.php?user=admin&page=add_user&do=add"; ?>" method="post">
        <div class="row">
            <div class="col-25">
                <h4>THÔNG TIN CHUNG</h4>
            </div>
        </div>

        <div class="row">
            <div class="col-25">
                <label for="uname"><b>Tên đăng nhập</b></label>
            </div>
            <div class="col-50">
                <input type="text" placeholder="username" name="username" id="username" value="<?php echo $data['username'] ?>" pattern="[a-zA-Z0-9]{0,}" required>
            </div>
            <div class="col-25">
                <?php if (isset($page_data["error"])) echo "<p style='color: red; margin-left: 20px'>" . $page_data["error"] . "</p>" ?>
            </div>
        </div>

        <div class="row">
            <div class="col-25">
                <label for="uname"><b>Họ và tên</b></label>
            </div>
            <div class="col-50">
                <input type="text" placeholder="Hồ Xuân Hùng" name="name" id="name" value="<?php echo $data['name'] ?>" pattern="[a-zA-Z ]{0,}" required>
            </div>
        </div>

        <div class="row">
            <div class="col-25">
                <label for="uname"><b>Số điện thoại</b></label>
            </div>
            <div class="col-50">
                <input type="text" placeholder="0339942764" name="phone" id="phone" value="<?php echo $data['phone'] ?>" pattern="+?[0-9]{0,}" required>
            </div>
        </div>

        <div class="row">
            <div class="col-25">
                <label for="uname"><b>Email</b></label>
            </div>
            <div class="col-50">
                <input type="text" placeholder="xuanhung@gmail.com" name="email" id="email" value="<?php echo $data['email'] ?>" pattern="[0-9a-zA-Z-_]+@[a-zA-Z]+.+[a-zA-Z]{2,4}" required>
            </div>
        </div>

        <div class="row">
            <div class="col-25">
                <label for="uname"><b>CMND/CCCD</b></label>
            </div>
            <div class="col-50">
                <input type="text" placeholder="1887281721837" name="id_card" id="id_card" value="<?php echo $data['id_card'] ?>" pattern="[0-9]+" required>
            </div>
        </div>

        <div class="row">
            <div class="col-25">
                <label for="uname"><b>Mật khẩu</b></label>
            </div>
            <div class="col-50">
                <input type="text" name="password" id="password" pattern="[a-zA-Z0-9!@#$%^&*_-]{6,}" required>
            </div>
        </div>

        <br>
        <div class="row">
            <div class="col-75 displayFlex">
                <input class="marginAuto" type="submit" value="Thêm">
            </div>
        </div>
    </form>
</div>
<br>
<br>