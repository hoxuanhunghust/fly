<div class="contentLogin">
    <form action="<?php echo WEB_PATH . "index.php?user=login&page=verify_login"; ?>" method="post" class="marginAuto">

        <div class="row">
            <div class="displayFlex">
                <img class="marginAuto" style="max-width: 30%" src="./assets/images/may-bay.jpg" />
            </div>
        </div>
        <br>

        <div class="row marginAuto" style="width:50%">
            <div class="col-50">
                <label for="uname"><b>Username</b></label></div>
            <div class="col-50">
                <input type="text" placeholder="Tên đăng nhập" name="username" id="username" required>
            </div>
        </div>
        <div class="row marginAuto" style="width:50%">
            <div class="col-50">
                <label for="psw"><b>Password</b></label>
            </div>
            <div class="col-50">
                <input type="password" placeholder="Mật khẩu" name="password" id="username" required>
            </div>
        </div>

        <div class="row marginAuto" style="width:50%; height: 30px; color:red">
            <?php echo $page_data["error"]; ?>
        </div>

        <div class="row marginAuto" style="width:50%">
            <div class="displayFlex">
                <button class="marginAuto" type="submit">Login</button>
            </div>
        </div>
        <br>
        <div class="row marginAuto" style="width:50%">
            <div class="register">
                <p>Bạn chưa có tài khoản?
                    <a class="" href="<?php echo WEB_PATH . "index.php?user=login&page=register"; ?>">
                        Đăng ký tại đây.
                    </a>
                </p>
            </div>
        </div>
</div>
</form>
</div>

<script>
    document.addEventListener('DOMContentLoaded', (event) => {
        document.getElementsByClassName("content")[0].setAttribute("style", "margin: 0px");
    })
    document.getElementById("username").onkeyup = function() {
        var $this = (this);
        var input = $this.value;
        var input = input.replace(/[^A-Z0-9]/ig, "");
        $this.value = input;
    };
</script>