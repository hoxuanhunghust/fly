<br>
<br>
<br>
<?php if (isset($page_data["success"])) if ($page_data["success"]) echo "<h4 style='color: green'>Đăng ký thông tin thành công</h4>" ?>
<?php if (isset($page_data["error"])) echo "<p style='color: red; margin-left: 20px'>" . $page_data["error"] . "</p>" ?>
<?php if (isset($page_data["username"])) $data['username'] = $page_data['username'] ?>
<?php if (isset($page_data["name"])) $data['name'] = $page_data['name'] ?>
<?php if (isset($page_data["phone"])) $data['phone'] = $page_data['phone'] ?>
<?php if (isset($page_data["email"])) $data['email'] = $page_data['email'] ?>
<?php if (isset($page_data["id_card"])) $data['id_card'] = $page_data['id_card'] ?>

<div class="contentLogin">
    <!-- <div class="container"> -->
    <form class="marginAuto" action="<?php echo WEB_PATH . "index.php?user=login&page=register&do=register"; ?>" method="post" style="width: 50%">
        <label>THÔNG TIN CHUNG</label>
        <div class="row">
            <div class="col-25">
                <label for="uname"><b>Tên đăng nhập</b></label>
            </div>
            <div class="col-50">
                <input type="text" placeholder="Tên đăng nhập" name="username" id="username" value="<?php echo $data['username'] ?>" pattern="[a-zA-Z0-9]{0,}" required>
            </div>

        </div>

        <div class="row">
            <div class="col-25">
                <label for="uname"><b>Họ và tên</b></label>
            </div>
            <div class="col-50">
                <input type="text" placeholder="Ho Xuan Hung" name="name" id="name" value="<?php echo $data['name'] ?>" required>
            </div>
        </div>

        <div class="row">
            <div class="col-25">
                <label for="uname"><b>Số điện thoại</b></label>
            </div>
            <div class="col-50">
                <input type="text" placeholder="0339942764" name="phone" id="phone" value="<?php echo $data['phone'] ?>" pattern="+?[0-9]{0,}" required>
            </div>
        </div>

        <div class="row">
            <div class="col-25">
                <label for="uname"><b>Email</b></label>
            </div>
            <div class="col-50">
                <input type="text" placeholder="xuanhung@gmail.com" name="email" id="email" value="<?php echo $data['email'] ?>" pattern="[0-9a-zA-Z-_]+@[a-zA-Z]+.+[a-zA-Z]{2,4}" required>
            </div>
        </div>

        <div class="row">
            <div class="col-25">
                <label for="uname"><b>CMND/CCCD</b></label>
            </div>
            <div class="col-50">
                <input type="text" placeholder="1887281721837" name="id_card" id="id_card" value="<?php echo $data['id_card'] ?>" pattern="[0-9]+" required>
            </div>
        </div>

        <div class="row">
            <div class="col-25">
                <label for="uname"><b>Mật khẩu</b></label>
            </div>
            <div class="col-50">
                <input type="password" name="password" pattern="[a-zA-Z0-9!@#$%^&*_-]{6,}" required>
            </div>
        </div>

        <div class="row">
            <div class="col-25">
                <label for="uname"><b>Nhập lại mật khẩu</b></label>
            </div>
            <div class="col-50">
                <input type="password" name="password2" pattern="[a-zA-Z0-9!@#$%^&*_-]{6,}" required>
            </div>
        </div>

        <br>
        <div class="row">
            <div class="col-25"></div>
            <div class="col-25">
                <input type="submit" value="Đăng ký">
            </div>
            <div class="col-25">
                <a href="<?php echo WEB_PATH . "index.php"; ?>">
                    <button class="edit" type="button">Trở lại</button>
                </a>
            </div>
        </div>
    </form>
    <!-- </div> -->
</div>
<br>
<br>

<script>
    document.addEventListener('DOMContentLoaded', (event) => {
        document.getElementsByClassName("content")[0].setAttribute("style", "margin: 0px");
    })
    document.getElementById("username").onkeyup = function() {
        var $this = (this);
        var input = $this.value;
        var input = input.replace(/[^A-Z0-9]/ig, "");
        $this.value = input;
    };
    document.getElementById("name").onkeyup = function() {
        var $this = (this);
        var input = $this.value;
        var input = input.replace(/[^A-Za-z ]/ig, "");
        $this.value = input;
    };
    document.getElementById("phone").onkeyup = function() {
        var $this = (this);
        var input = $this.value;
        var input = input.replace(/[^0-9+]/ig, "");
        $this.value = input;
    };
    document.getElementById("email").onkeyup = function() {
        var $this = (this);
        var input = $this.value;
        var input = input.replace(/[^a-zA-Z0-9@._-]/ig, "");
        $this.value = input;
    };
    document.getElementById("id_card").onkeyup = function() {
        var $this = (this);
        var input = $this.value;
        var input = input.replace(/[^0-9]/ig, "");
        $this.value = input;
    };
</script>
