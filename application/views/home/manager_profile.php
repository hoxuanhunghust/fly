<br>
<?php if(isset($page_data["success"])) if($page_data["success"]) echo "<h4 style='color: green'>Cập nhật thông tin thành công</h4>" ?>
<?php if(isset($page_data["error"])) if($page_data["error"]) echo "<h4 style='color: red'>Mật khẩu nhập lại không trùng</h4>" ?>
<?php if(isset($page_data["errorXSS"])) if($page_data["error"]) echo "<h4 style='color: red'>Vui lòng đảm bảo thông tin bạn nhập vào không có ký tự đặc biệt.</h4>" ?>
<?php
foreach ($page_data["profile"] as $data) {
?>

    <div class="container">
        <form action="<?php echo WEB_PATH . "index.php?user=home&page=manager_profile&do=update"; ?>" method="post">
            <label>THÔNG TIN CHUNG</label>
            <div class="row">
                <div class="col-25">
                    <label for="uname"><b>Tên đăng nhập</b></label>
                </div>
                <div class="col-75">
                    <input type="text" placeholder="Tên đăng nhập" name="username" id="username"  value="<?php echo $data['username'] ?>">
                </div>
            </div>

            <div class="row">
                <div class="col-25">
                    <label for="uname"><b>Họ và tên</b></label>
                </div>
                <div class="col-75">
                    <input type="text" placeholder="Hồ Xuân Hùng" name="name" value="<?php echo $data['name'] ?>">
                </div>
            </div>

            <div class="row">
                <div class="col-25">
                    <label for="uname"><b>Số điện thoại</b></label>
                </div>
                <div class="col-75">
                    <input type="text" placeholder="0339942764" name="phone" value="<?php echo $data['phone'] ?>">
                </div>
            </div>

            <div class="row">
                <div class="col-25">
                    <label for="uname"><b>Email</b></label>
                </div>
                <div class="col-75">
                    <input type="text" placeholder="xuanhung@gmail.com" name="email" value="<?php echo $data['email'] ?>">
                </div>
            </div>

            <div class="row">
                <div class="col-25">
                    <label for="uname"><b>CMND/CCCD</b></label>
                </div>
                <div class="col-75">
                    <input type="text" placeholder="1887281721837" name="id_card" value="<?php echo $data['id_card'] ?>">
                </div>
            </div>

            <hr>
            <label>ĐỔI MẬT KHẨU</label>
            <div class="row">
                <div class="col-25">
                    <label for="uname"><b>Mật khẩu</b></label>
                </div>
                <div class="col-75">
                    <input type="password" name="password">
                </div>
            </div>

            <div class="row">
                <div class="col-25">
                    <label for="uname"><b>Nhập lại mật khẩu</b></label>
                </div>
                <div class="col-75">
                    <input type="password" name="password2">
                </div>
            </div>

            <br>
            <div class="row">
                <div class="col-25">
                    <label for="uname"><b> </b></label>
                </div>
                <div class="col-75">
                    <input type="submit" value="Cập nhật">
                </div>
            </div>
        </form>
        <br>
        <br>
    </div>
<?php
}
?>
<script>
    document.getElementById("username").onkeyup = function() {
        var $this = (this);
        var input = $this.value;
        var input = input.replace(/[^A-Z0-9]/ig, "");
        $this.value = input;
    };
</script>