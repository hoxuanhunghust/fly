<br>
<?php if(isset($page_data["success"])) if($page_data["success"]) echo "<h2 style='color: green'>Đặt vé thành công</h2>" ?>
<?php if(isset($page_data["delete"])) if($page_data["delete"]) echo "<h2 style='color: red'>Huỷ vé thành công</h2>" ?>

<h1>DANH SÁCH CHUYẾN BAY ĐÃ ĐẶT</h1>
<table style="width:100%">
  <tr>
    <th>STT</th>
    <th>Tên chuyến bay</th>
    <th>Mã</th>
    <th>Giờ khởi hành</th>
    <th>Giờ đến</th>
    <th>Giá vé</th>
    <th>Ngày đặt vé</th>
    <th>Huỷ vé</th>
  </tr>
  <?php 
  $stt = 1;
  foreach($page_data["fly_list"] as $key => $value){ ?>
  <tr>
    <td><?php echo $stt++; ?></td>
    <td><?php echo $value["name"]; ?></td>
    <td><?php echo $value["code"]; ?></td>
    <td><?php echo $value["starttime"]; ?></td>
    <td><?php echo $value["endtime"]; ?></td>
    <td><?php echo $value["price"]; ?></td>
    <td><?php echo $value["time_book"]; ?></td>
    <td><a href=<?php echo WEB_PATH . "index.php?user=home&page=book_flight&do=delete&ticket_id=".$value["ticket_id"]; ?>><button class="delete">Huỷ vé ngay</button></a></td>
  </tr>
  <?php }?>
</table>
<br>
<br>