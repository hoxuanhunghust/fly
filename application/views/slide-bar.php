<!-- HOME SLIDEBAR -->
<?php if (($_SESSION["user_type"] == "home")) { ?>
    <div class="sidebar">
        <a <?php if ($_SESSION["page_name"] == "search_flight") echo 'class="active"'; ?> href="<?php echo WEB_PATH . "index.php?user=home&page=search_flight"; ?>">Đặt vé</a>
        <a <?php if ($_SESSION["page_name"] == "index") echo 'class="active"'; ?> href="<?php echo WEB_PATH . "index.php?user=home&page=index"; ?>">Chuyến bay đã đặt</a>
        <a <?php if ($_SESSION["page_name"] == "manager_profile") echo 'class="active"'; ?> href="<?php echo WEB_PATH . "index.php?user=home&page=manager_profile"; ?>">Tài khoản</a>
    </div>
<?php } ?>

<!-- ADMIN SLIDEBAR -->
<?php if (($_SESSION["user_type"] == "admin")) { ?>
    <div class="sidebar">
        <a <?php if ($_SESSION["page_name"] == "index") echo 'class="active"'; ?> href="<?php echo WEB_PATH . "index.php?user=admin&page=index"; ?>">Trang chủ</a>
        <a <?php if ($_SESSION["page_name"] == "manager_flight") echo 'class="active"'; ?> href="<?php echo WEB_PATH . "index.php?user=admin&page=manager_flight"; ?>">Danh sách chuyến bay</a>
        <a <?php if ($_SESSION["page_name"] == "manager_profile") echo 'class="active"'; ?> href="<?php echo WEB_PATH . "index.php?user=admin&page=manager_profile"; ?>">Tài khoản</a>
    </div>
<?php } ?>

