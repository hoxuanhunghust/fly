<?php

class Login extends Controller
{
    public function index()
    {
        if ($this->user_data->getUserData("user_type") != "") {
            $this->redirect($this->user_data->getUserData("user_type"));
        }

        $page_data["error"] = '';
        $this->loader->view_('login', $page_data);
    }

    public function verify_login()
    {
        if ($this->user_data->getUserData("user_type") != "") {
            $this->redirect($this->user_data->getUserData("user_type"));
        }

        if (isset($_POST['username'])) $data['username'] = $_POST['username'];
        if (isset($_POST['password'])) $data['password'] = md5($_POST['password']);
        $this->loader->model("UserModel");
        $login = new UserModel();
        $user = $login->check_login($data);
        if (!$user[0]["user_id"]) {
            $page_data['error'] = "*Sai tên đăng nhập hoặc mật khẩu";
            $this->loader->view_('login', $page_data);
        } else {
            echo "Đúng";
            $this->user_data->setUserData("user_id", $user[0]['user_id']);
            $this->user_data->setUserData("user_name", $user[0]['username']);
            $this->user_data->setUserData("name", $user[0]['name']);
            $this->user_data->setUserData("user_type", $user[0]['type']);
            $this->redirect($this->user_data->getUserData("user_type"));
        }
    }

    public function register()
    {
        if ($this->user_data->getUserData("user_type") != "") {
            $this->redirect('login');
        }

        $page_data = array();
        $data = array();
        $this->loader->model("UserModel");
        $user = new UserModel();
        if (isset($_REQUEST['do']) && ($_REQUEST['do'] == 'register')) {
            if (isset($_POST['username'])) $data['username'] = $_POST['username'];
            if (isset($_POST['name'])) $data['name'] = $_POST['name'];
            if (isset($_POST['phone'])) $data['phone'] = $_POST['phone'];
            if (isset($_POST['email'])) $data['email'] = $_POST['email'];
            if (isset($_POST['id_card'])) $data['id_card'] = $_POST['id_card'];

            if(($user->getUsers($data['username'])) || ($user->getAdmins($data['username']))){
                $page_data["error"] = "*Tên đăng nhập đã tồn tại";
                $page_data['username'] = $_POST['username'];
                $page_data['name'] = $_POST['name'];
                $page_data['phone'] = $_POST['phone'];
                $page_data['email'] = $_POST['email'];
                $page_data['id_card'] = $_POST['id_card'];
            }
            else if(($user->getEmailUsers($data['email']))){
                $page_data["error"] = "*Email đã tồn tại";
                $page_data['username'] = $_POST['username'];
                $page_data['name'] = $_POST['name'];
                $page_data['phone'] = $_POST['phone'];
                $page_data['email'] = $_POST['email'];
                $page_data['id_card'] = $_POST['id_card'];
            }
            else if (($_POST['password'] === $_POST['password2'])) {
                send_new_account($data['name'], $_POST['password'], $data['phone'], $data['email']);
                $data['password'] = md5($_POST['password']);
                $page_data["success"] = $user->addUser($data);
                // $this->redirect('index');
            } else{
                $page_data["error"] = "*Mật khẩu không trùng khớp";
                $page_data['username'] = $_POST['username'];
                $page_data['name'] = $_POST['name'];
                $page_data['phone'] = $_POST['phone'];
                $page_data['email'] = $_POST['email'];
                $page_data['id_card'] = $_POST['id_card'];
            }
        }
        $page_data["profile"] = $user->getUsers($this->user_data->getUserData("user_name"));
        $page_data["page_name"] = "register";
        $this->loader->view_('register', $page_data);
    }

    public function logout()
    {
        $this->user_data->deleteUserData("user_id");
        $this->user_data->deleteUserData("user_name");
        $this->user_data->deleteUserData("name");
        $this->user_data->deleteUserData("user_type");
        $this->redirect("login");
    }
}
