<?php

class Home extends Controller
{
    public function index()
    {
        if ($this->user_data->getUserData("user_type") != "home") {
            $this->redirect('login');
        }
        $this->loader->model("FlyModel");
        $fly = new FlyModel();
        $page_data["fly_list"] = $fly->getFlyList($this->user_data->getUserData("user_id"));
        $page_data["page_name"] = "index";
        $this->loader->views('index', $page_data);
    }

    public function search_flight()
    {
        if ($this->user_data->getUserData("user_type") != "home") {
            $this->redirect('login');
        }
        $page_data = array();
        $code = '';
        $starttime = '';
        $endtime = '';
        $name = '';
        $price = '';
        $this->loader->model("FlyModel");
        $fly = new FlyModel();

        // Tìm kiếm
        if (isset($_REQUEST['do']) && ($_REQUEST['do'] == 'search')) {
            if (isset($_POST['code'])) $code = $_POST['code'];
            if (isset($_POST['starttime'])) $starttime = $_POST['starttime'];
            if (isset($_POST['endtime'])) $endtime = $_POST['endtime'];
            if (isset($_POST['price'])) $price = str_replace(",", "", $_POST['price']);
            if (isset($_POST['name'])) $name = $_POST['name'];
        }

        // Đặt vé
        if (isset($_REQUEST['do']) && ($_REQUEST['do'] == 'book')) {
            $flight_id = $_REQUEST['flight_id'];
            $time_book = date("Y-m-d H:i:s");
            $page_data["success"] = $fly->BookFlight($this->user_data->getUserData("user_id"), $flight_id, $time_book);
        }

        $flight_lists = $fly->SearchFlight("", "", "", "", "");
        foreach($flight_lists as $key => $value){
            $page_data["flight_name_lists"][] = $value["name"];
            $page_data["flight_price_lists"][] = $value["price"];
            $page_data["flight_code_lists"][] = $value["code"];
        }

        $page_data["fly_list"] = $fly->SearchFlight($code, $starttime, $endtime, $name, $price);
        $page_data["page_name"] = "search_flight";
        $this->loader->views('search', $page_data);
    }

    public function manager_profile()
    {
        if ($this->user_data->getUserData("user_type") != "home") {
            $this->redirect('login');
        }
        $page_data = array();
        $data = array();
        $this->loader->model("UserModel");
        $user = new UserModel();
        if (isset($_REQUEST['do']) && ($_REQUEST['do'] == 'update')) {
            if (isset($_POST['name'])){
                if(substr_count($_POST['name'], "<", 0) > 0){
                    $page_data["errorXSS"] = "Co the bi tan cong XSS";
                }
                else{
                    $data['name'] = $_POST['name'];
                }
            }
            if (isset($_POST['phone'])){
                if(substr_count($_POST['phone'], "<", 0) > 0){
                    $page_data["errorXSS"] = "Co the bi tan cong XSS";
                }
                else{
                    $data['phone'] = $_POST['phone'];
                }
            } 
            if (isset($_POST['email'])){
                if(substr_count($_POST['email'], "<", 0) > 0){
                    $page_data["errorXSS"] = "Co the bi tan cong XSS";
                }
                else{
                    $data['email'] = $_POST['email'];
                }
            }
            if (isset($_POST['id_card'])){
                if(substr_count($_POST['id_card'], "<", 0) > 0){
                    $page_data["errorXSS"] = "Co the bi tan cong XSS";
                }
                else{
                    $data['id_card'] = $_POST['id_card'];
                }
            }
            if (isset($_POST['password'])){
                if(substr_count($_POST['password'], "<", 0) > 0){
                    $page_data["errorXSS"] = "Co the bi tan cong XSS";
                }
            }
            if (isset($_POST['password2'])){
                if(substr_count($_POST['password2'], "<", 0) > 0){
                    $page_data["errorXSS"] = "Co the bi tan cong XSS";
                }
            }
            
            if ($_POST['password'] != '') {
                $password = $_POST['password'];
                $password2 = $_POST['password2'];
                if (($password != $password2)) {
                    $page_data["error"] = "Mật khẩu nhập lại không trùng!";
                } else {
                    $data['password'] = md5($password);
                    $page_data["success"] = $user->updateInfo($this->user_data->getUserData("user_id"), $data);
                }
            } else {
                $page_data["success"] = $user->updateInfo($this->user_data->getUserData("user_id"), $data);
            }
        }
        $page_data["profile"] = $user->getUsers($this->user_data->getUserData("user_name"));
        $page_data["page_name"] = "manager_profile";
        $this->loader->views('manager_profile', $page_data);
    }

    public function book_flight()
    {
        if ($this->user_data->getUserData("user_type") != "home") {
            $this->redirect('login');
        }
        $this->loader->model("FlyModel");
        $fly = new FlyModel();

        // Đặt vé
        if (
            isset($_REQUEST['do']) && ($_REQUEST['do'] == 'book') &&
            isset($_REQUEST['flight_id'])) {
            $flight_id = $_REQUEST['flight_id'];
            $time_book = date("Y-m-d H:i:s");
            $page_data["success"] = $fly->BookFlight($this->user_data->getUserData("user_id"), $flight_id, $time_book);
            $page_data["fly_list"] = $fly->getFlyList($this->user_data->getUserData("user_id"));
            
            $page_data["page_name"] = "index";
            $this->loader->views('index', $page_data);
        }

        // Huỷ vé
        else if (
            isset($_REQUEST['do']) && ($_REQUEST['do'] == 'delete') &&
            isset($_REQUEST['ticket_id'])) {
            $ticket_id = $_REQUEST['ticket_id'];
            $page_data["delete"] = $fly->DeleteFlight($ticket_id);
            $page_data["fly_list"] = $fly->getFlyList($this->user_data->getUserData("user_id"));
            $page_data["page_name"] = "index";
            $this->loader->views('index', $page_data);
        } else $this->redirect('login');
    }
}
