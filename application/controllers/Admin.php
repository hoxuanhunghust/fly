<?php

class Admin extends Controller
{
    public function index()
    {
        if ($this->user_data->getUserData("user_type") != "admin") {
            $this->redirect('login');
        }
        $this->loader->model("UserModel");
        $fly = new UserModel();
        $page_data["user_list"] = $fly->getUsersList();
        $page_data["page_name"] = "index";
        $this->loader->views('index', $page_data);
    }

    public function manager_user()
    {
        if ($this->user_data->getUserData("user_type") != "admin") {
            $this->redirect('login');
        }
        $page_data = array();
        $data = array();
        $this->loader->model("UserModel");
        $user = new UserModel();

        if (isset($_REQUEST['do']) && ($_REQUEST['do'] == 'add')) {
            if (isset($_POST['username'])) $data['username'] = $_POST['username'];
            if (isset($_POST['name'])) $data['name'] = $_POST['name'];
            if (isset($_POST['phone'])) $data['phone'] = $_POST['phone'];
            if (isset($_POST['email'])) $data['email'] = $_POST['email'];
            if (isset($_POST['id_card'])) $data['id_card'] = $_POST['id_card'];
            $page_data["success"] = $user->addUser($data);
            $page_data["profile"] = $user->getUsers($data['username']);
        }

        if (isset($_REQUEST['do']) && ($_REQUEST['do'] == 'update')) {
            if (isset($_POST['name'])) $data['name'] = $_POST['name'];
            if (isset($_POST['phone'])) $data['phone'] = $_POST['phone'];
            if (isset($_POST['email'])) $data['email'] = $_POST['email'];
            if (isset($_POST['id_card'])) $data['id_card'] = $_POST['id_card'];
            $page_data["success"] = $user->updateInfo($_REQUEST['user_id'], $data);
            $page_data["profile"] = $user->getUsersById($_REQUEST['user_id']);
        }

        if (isset($_REQUEST['do']) && ($_REQUEST['do'] == 'edit')) {
            $page_data["profile"] = $user->getUsers($_REQUEST['user_name']);
        }

        if (isset($_REQUEST['do']) && ($_REQUEST['do'] == 'delete')) {
            $page_data["delete"] = $user->deleteUsers($_REQUEST['user_id']);
            $this->redirect('index');
        }

        $page_data["page_name"] = "manager_user";
        $this->loader->views('manager_user', $page_data);
    }

    public function add_user()
    {
        if ($this->user_data->getUserData("user_type") != "admin") {
            $this->redirect('login');
        }
        $page_data = array();
        $data = array();
        $this->loader->model("UserModel");
        $user = new UserModel();

        if (isset($_REQUEST['do']) && ($_REQUEST['do'] == 'add')) {
            if (isset($_POST['username'])) $data['username'] = $_POST['username'];
            if (isset($_POST['name'])) $data['name'] = $_POST['name'];
            if (isset($_POST['phone'])) $data['phone'] = $_POST['phone'];
            if (isset($_POST['email'])) $data['email'] = $_POST['email'];
            if (isset($_POST['id_card'])) $data['id_card'] = $_POST['id_card'];
            if (isset($_POST['password'])) $data['password'] = md5($_POST['password']);

            if(($user->getUsers($data['username'])) || ($user->getAdmins($data['username']))){
                $page_data["error"] = "*Tên đăng nhập đã tồn tại";
                $page_data['username'] = $_POST['username'];
                $page_data['name'] = $_POST['name'];
                $page_data['phone'] = $_POST['phone'];
                $page_data['email'] = $_POST['email'];
                $page_data['id_card'] = $_POST['id_card'];
            }
            else if(($user->getEmailUsers($data['email']))){
                $page_data["error"] = "*Email đã tồn tại";
                $page_data['username'] = $_POST['username'];
                $page_data['name'] = $_POST['name'];
                $page_data['phone'] = $_POST['phone'];
                $page_data['email'] = $_POST['email'];
                $page_data['id_card'] = $_POST['id_card'];
            }
            else{
                $page_data["success"] = $user->addUser($data);
                $this->redirect('index');
            }
        }
        $page_data["page_name"] = "add_user";
        $this->loader->views('add_user', $page_data);
    }
    public function history_booking()
    {
        if ($this->user_data->getUserData("user_type") != "admin") {
            $this->redirect('login');
        }
        $this->loader->model("FlyModel");
        $fly = new FlyModel();
        $page_data["fly_list"] = $fly->getFlyList($_REQUEST['user_id']);
        $page_data["page_name"] = "history_booking";
        $this->loader->views('history_booking', $page_data);
    }

    public function book_flight()
    {
        if ($this->user_data->getUserData("user_type") != "admin") {
            $this->redirect('login');
        }
        $this->loader->model("FlyModel");
        $fly = new FlyModel();
        // Huỷ vé
        $page_data["delete"] = $fly->DeleteFlight($_REQUEST['ticket_id']);
        $this->redirect('admin&page=history_booking&user_id=' . $_REQUEST['user_id']);
    }

    public function manager_flight()
    {
        if ($this->user_data->getUserData("user_type") != "admin") {
            $this->redirect('login');
        }
        $page_data = array();
        $code = '';
        $starttime = '';
        $endtime = '';
        $name = '';
        $price = '';
        $this->loader->model("FlyModel");
        $fly = new FlyModel();

        // Tìm kiếm
        if (isset($_REQUEST['do']) && ($_REQUEST['do'] == 'search')) {
            if (isset($_POST['code'])) $code = $_POST['code'];
            if (isset($_POST['starttime'])) $starttime = $_POST['starttime'];
            if (isset($_POST['endtime'])) $endtime = $_POST['endtime'];
            if (isset($_POST['price'])) $price = str_replace(",", "", $_POST['price']);
            if (isset($_POST['name'])) $name = $_POST['name'];
        }

        if (isset($_REQUEST['do']) && ($_REQUEST['do'] == 'edit')) {
            $page_data["profile"] = $fly->getFlight($_REQUEST['flight_id']);
        }

        if (isset($_REQUEST['do']) && ($_REQUEST['do'] == 'delete')) {
            $page_data["delete"] = $fly->deleteFly($_REQUEST['flight_id']);
        }

        $flight_lists = $fly->SearchFlight("", "", "", "", "");
        foreach ($flight_lists as $key => $value) {
            $page_data["flight_name_lists"][] = $value["name"];
            $page_data["flight_price_lists"][] = $value["price"];
            $page_data["flight_code_lists"][] = $value["code"];
        }

        $page_data["flight"] = $fly->SearchFlight($code, $starttime, $endtime, $name, $price);
        $page_data["page_name"] = "manager_flight";
        $this->loader->views('manager_flight', $page_data);
    }

    public function edit_flight()
    {
        if ($this->user_data->getUserData("user_type") != "admin") {
            $this->redirect('login');
        }

        $this->loader->model("FlyModel");
        $fly = new FlyModel();

        if (isset($_REQUEST['do']) && ($_REQUEST['do'] == 'update')) {
            if (isset($_POST['code'])) $data['code'] = $_POST['code'];
            if (isset($_POST['name'])) $data['name'] = $_POST['name'];
            if (isset($_POST['starttime'])) $data['starttime'] = $_POST['starttime'];
            if (isset($_POST['endtime'])) $data['endtime'] = $_POST['endtime'];
            if (isset($_POST['price'])) $data['price'] = str_replace(",", "", $_POST['price']);
            $page_data["success"] = $fly->updateInfo($_REQUEST['flight_id'], $data);
            $page_data["profile"] = $fly->getFlight($_REQUEST['flight_id']);
        }

        if (isset($_REQUEST['do']) && ($_REQUEST['do'] == 'edit')) {
            $page_data["profile"] = $fly->getFlight($_REQUEST['flight_id']);
        }

        $page_data["page_name"] = "edit_flight";
        $this->loader->views('edit_flight', $page_data);
    }

    public function add_flight()
    {
        if ($this->user_data->getUserData("user_type") != "admin") {
            $this->redirect('login');
        }

        $this->loader->model("FlyModel");
        $fly = new FlyModel();

        if (isset($_REQUEST['do']) && ($_REQUEST['do'] == 'add')) {
            if (isset($_POST['code'])) $data['code'] = $_POST['code'];
            if (isset($_POST['name'])) $data['name'] = $_POST['name'];
            if (isset($_POST['starttime'])) $data['starttime'] = $_POST['starttime'];
            if (isset($_POST['endtime'])) $data['endtime'] = $_POST['endtime'];
            if (isset($_POST['price'])) $data['price'] = str_replace(",", "", $_POST['price']);
            // echo var_dump($data); die;
            $page_data["success"] = $fly->insertInfo($data);
            $this->redirect('admin&page=manager_flight');
        }
        $page_data["profile"] = array();
        $page_data["page_name"] = "add_flight";
        $this->loader->views('add_flight', $page_data);
    }

    public function manager_profile()
    {
        if ($this->user_data->getUserData("user_type") != "admin") {
            $this->redirect('login');
        }
        
        $page_data = array();
        $data = array();
        $this->loader->model("UserModel");
        $user = new UserModel();

        if (isset($_REQUEST['do']) && ($_REQUEST['do'] == 'update')) {
            if (isset($_POST['name'])) $data['name'] = $_POST['name'];
            if (isset($_POST['phone'])) $data['phone'] = $_POST['phone'];

            if ($_POST['password'] != '') {
                $password = $_POST['password'];
                $password2 = $_POST['password2'];
                if (($password != $password2)) {
                    $page_data["error"] = "Mật khẩu nhập lại không trùng!";
                } else {
                    $data['password'] = md5($password);
                    $page_data["success"] = $user->updateAdminInfo($this->user_data->getUserData("user_id"), $data);
                }
            } else {
                $page_data["success"] = $user->updateAdminInfo($this->user_data->getUserData("user_id"), $data);
            }
        }
        $page_data["profile"] = $user->getAdmin($this->user_data->getUserData("user_id"));
        // echo $this->user_data->getUserData("user_id"). "--" . var_dump($page_data["profile"]); die;
        $page_data["page_name"] = "manager_profile";
        $this->loader->views('manager_profile', $page_data);
    }
}
