<?php

class UserModel extends Model
{

    public function check_login($data)
    {
        $user = $this->select("user", $data);
        if(!$user){
            $user = $this->select("admin", $data);
            $user[0]["type"] = "admin";
        } else{
            $user[0]["type"] = "home";
        }
        return $user;
    }

    public function updateInfo($user_id, $data)
    {
        $users = $this->update("user", array("user_id" => $user_id), $data);
        return $users;
    }

    public function addUser($data)
    {
        $users = $this->insert("user", $data);
        return $users;
    }

    public function getUsers($username)
    {
        $users = $this->select("user", array("username" => $username));
        return $users;
    }
    public function getUsersById($user_id)
    {
        $users = $this->select("user", array("user_id" => $user_id));
        return $users;
    }

    public function getAdmins($username)
    {
        $admin = $this->select("admin", array("username" => $username));
        return $admin;
    }

    public function getEmailUsers($email)
    {
        $users = $this->select("user", array("email" => $email));
        return $users;
    }

    public function getUsersList()
    {
        $users = $this->selectAll("user");
        return $users;
    }
    public function deleteUsers($user_id)
    {
        $users = $this->delete("user", array("user_id" => $user_id));
        return $users;
    }

    public function getAdmin($user_id)
    {
        $users = $this->select("admin", array("user_id" => $user_id));
        return $users;
    }

    public function updateAdminInfo($user_id, $data)
    {
        $users = $this->update("admin", array("user_id" => $user_id), $data);
        return $users;
    }
}
