<?php

class FlyModel extends Model
{

    public function getFlyList($user_id)
    {
        $sql = "SELECT * FROM flight, ticket WHERE flight.flight_id = ticket.flight_id
        AND user_id = '" . $user_id . "'";
        $result = $this->db->query($sql);
        $list = array();
        while ($row = $result->fetch_assoc()) {
            $list[] = $row;
        }
        return $list;
    }

    public function getFlight($flight_id)
    {
        $flight = $this->select("flight", array("flight_id" => $flight_id));
        return $flight;
    }
    
    public function insertInfo($data)
    {
        $flight = $this->insert("flight", $data);
        return $flight;
    }

    public function deleteFly($flight_id)
    {
        $flight = $this->delete("flight", array("flight_id" => $flight_id));
        return $flight;
    }

    public function updateInfo($flight_id, $data)
    {
        $flight = $this->update("flight", array("flight_id" => $flight_id), $data);
        return $flight;
    }

    public function SearchFlight($code, $starttime, $endtime, $name, $price)
    {
        $data = array();
        if ($code) $data["code"] = $code;
        if ($starttime != '' and $endtime != '') {
            $data["starttime <"] = $starttime;
            $data["endtime >"] = $starttime;
        }
        if ($name) $data["name"] = '%'.$name.'%';
        if ($price) $data["price"] = $price;

        $user = $this->select("flight", $data);
        return $user;
    }

    public function BookFlight($user, $flight_id, $time_book)
    {
        $data = array();
        $data["user_id"] = $user;
        $data["flight_id"] = $flight_id;
        $data["time_book"] = $time_book;
        $user = $this->insert("ticket", $data);
        return $user;
    }

    public function DeleteFlight($ticket_id)
    {
        $data = array();
        $data["ticket_id"] = $ticket_id;
        $user = $this->delete("ticket", $data);
        return $user;
    }
    
}
