document.getElementById("price").onkeyup = function() {
    var $this = (this);
    var input = $this.value;
    var input = input.replace(/[\D\s,\._\-]+/g, "");
    input = input ? parseInt(input, 10) : 0;
    console.log(input.toLocaleString("en-US"));
    $this.value = input.toLocaleString("en-US");
};

document.getElementById("username").onkeyup = function() {
    var $this = (this);
    var input = $this.value;
    var input = input.replace(/[^A-Z0-9]/ig, "");
    $this.value = input;
};