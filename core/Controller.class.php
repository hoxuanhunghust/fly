<?php
class Controller
{
    protected $loader;
    public $user_data;

    public function __construct()
    {
        $this->loader = new Loader();
        $this->user_data = new UserData();
    }

    public function redirect($user)
    {
        header("Location:" . WEB_PATH . "index.php?user=$user");
        // else header("Location:" .WEB_PATH. "application/views/404.php");
        exit;
    }
}