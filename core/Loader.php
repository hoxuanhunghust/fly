<?php
class Loader
{
    public function library($lib)
    {
        include LIB_PATH . "$lib.class.php";
    }

    public function view($view)
    {
        include CURR_VIEW_PATH . "$view.php";
    }

    public function views($view, $page_data)
    {   
        foreach ($page_data as $key => $value) {
        }
        if ($page_data["page_name"] != NULL)
            $_SESSION["page_name"] = $page_data["page_name"];
        if (file_exists(CURR_VIEW_PATH . "$view.php")) require CURR_VIEW_PATH . "$view.php";
        else require VIEW_PATH . "404.php";
    }
    public function view_($view, $page_data)
    {
        foreach ($page_data as $key => $value) {
        }
        require VIEW_PATH . "$view.php";
    }

    public function model($model)
    {
        include MODEL_PATH . "$model.class.php";
    }
}
