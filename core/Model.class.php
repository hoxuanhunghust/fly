<?php
class Model
{

    protected $db;
    protected $table;
    protected $fields = array();

    public function __construct()
    {
        $dbconfig['host'] = $GLOBALS['config']['host'];
        $dbconfig['user'] = $GLOBALS['config']['user'];
        $dbconfig['password'] = $GLOBALS['config']['password'];
        $dbconfig['database'] = $GLOBALS['config']['database'];
        $dbconfig['port'] = $GLOBALS['config']['port'];
        $dbconfig['charset'] = $GLOBALS['config']['charset'];

        $this->db = new Mysql($dbconfig);
    }

    public function selectAll($table)
    {
        $sql = "SELECT * FROM `{$table}`";
        // echo $sql;
        $result = $this->db->getResultArray($sql);
        if ($result) return $result;
        else return false;
    }

    public function select($table, $data)
    {
        $where = "1=1 ";
        foreach ($data as $k => $v) {
            $k = $this->db->mysqli_real_escape_string_ok($k);
            $v = $this->db->mysqli_real_escape_string_ok($v);
            if(strpos($v, "%") === false) $where .= " AND " . $k . "= '" . $v . "'";
            else $where .= " AND " . $k . " LIKE '" . $v . "'";
        }
        
        $sql = "SELECT * FROM `{$table}` WHERE {$where}";
        // echo $sql; 
        $result = $this->db->getResultArray($sql);
        if ($result) return $result;
        else return false;
    }

    public function insert($table, $data)
    {
        $field_list = '';
        $value_list = '';
        foreach ($data as $k => $v) {
            $k = $this->db->mysqli_real_escape_string_ok($k);
            $v = $this->db->mysqli_real_escape_string_ok($v);
            $field_list .= "`" . $k . "`" . ',';
            $value_list .= "'" . $v . "'" . ',';
        }
        $field_list = rtrim($field_list, ',');
        $value_list = rtrim($value_list, ',');

        $sql = "INSERT INTO `{$table}` ({$field_list}) VALUES ($value_list)";

        // Nếu thành công thì trả lại id của dòng đã insert
        if ($this->db->query($sql)) return $this->db->getInsertId();
        else return false;
    }

    public function update($table, $condition, $data)
    {
        $set = '';
        $where = "1=1 ";
        foreach ($condition as $k => $v) {
            $k = $this->db->mysqli_real_escape_string_ok($k);
            $v = $this->db->mysqli_real_escape_string_ok($v);
            $where .= " AND `$k`='$v'";
        }
        foreach ($data as $k2 => $v2) {
            $k = $this->db->mysqli_real_escape_string_ok($k);
            $v = $this->db->mysqli_real_escape_string_ok($v);
            $set .= " `$k2`='$v2',";
        }
        $set = rtrim($set, ',');
        $sql = "UPDATE `{$table}` SET {$set} WHERE {$where}";
        // echo $sql; die;
        // Nếu thành công thì trả lại true
        if ($this->db->query($sql)) {
            return true;
        } else {
            return false;
        }
    }

    public function delete($table, $data)
    {
        $where = "1=1 ";
        foreach ($data as $k => $v) {
            $k = $this->db->mysqli_real_escape_string_ok($k);
            $v = $this->db->mysqli_real_escape_string_ok($v);
            $where .= " AND `" . $k . "` = '" . $v . "'";
        }
        $sql = "DELETE FROM `{$table}` WHERE {$where}";
        // echo $sql; die;
        $result = $this->db->query($sql);
        if ($result) return $result;
        else return false;
    }

    // Đếm tổng
    public function total()
    {
        $table = $this->db->mysqli_real_escape_string_ok($this->table);
        $sql = "select count(*) from {$this->table}";
        return $this->db->getFirstCollum($sql);
    }

    public function selectUsername($table)
    {
        $sql = "SELECT `username` FROM `{$table}`" ;
        // echo $sql;
        $result = $this->db->getResultArray($sql);
        if ($result) return $result;
        else return false;
    }
}
