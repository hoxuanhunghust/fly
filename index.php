<head>
    <title>Web đặt vé máy bay trực tuyến</title>
    <link rel="stylesheet" href="./assets/css/main.css">
    <!-- <link href='https://fonts.googleapis.com/css?family=Nunito' rel='stylesheet'> -->
    <link href='./assets/css/nunito.css' rel='stylesheet' type='text/css'>
    <link rel="icon" href="./assets/images/bk.png" />
    <link rel="shortcut icon" href="./assets/images/bk.png" />
</head>

<body>
    <?php
    // INIT SETTING
    error_reporting(E_ALL ^ E_NOTICE);
    require "config/setting.php";
    require "config/config.php";
    
    ?>

    <div class="content">
        <?php
        // Phần thân web
        $controller_name = CONTROLLER;
        $action_name = ACTION;
        $controller = new $controller_name;
        if(method_exists($controller, $action_name)) $controller->$action_name();
        else  require VIEW_PATH . "404.php";
        ?>
    </div>

    <?php
    // HEADER, FOOTER, SLIDEBAR
    include "application/views/slide-bar.php";
    include "application/views/header.php";
    include "application/views/footer.php";
    ?>
    <script src="./assets/js/main.js"></script>

</body>