<?php
class Mysql
{
    protected $conn = false;
    protected $sql;

    public function __construct($config = array())
    {
        $host = $config['host'];
        $user = $config['user'];
        $password = $config['password'];
        $database = $config['database'];
        $port = $config['port'];
        $charset = $config['charset'];
        
        $this->conn = new mysqli($host.':'.$port, $user, $password, $database);
        if ($this->conn->connect_errno) {
            die("Kết nối Database thất bại: " . $this->conn->connect_error);
        }
        $this->conn->set_charset($charset);
    }

    public function query($sql)
    {
        $result = $this->conn->query($sql);
        if (!$result) die("Error: " . $sql . "<br>" . mysqli_error($this->conn));
        // echo var_dump($result);
        return $result;
    }

    public function mysqli_real_escape_string_ok($sql)
    {
        $sql = str_replace('/[^0-9]/', '', $sql);
        $sql = mysqli_real_escape_string($this->conn, $sql);
        return $sql;
    }

    public function getFirstCollum($sql)
    {
        $result = $this->query($sql);
        $row = $result->fetch_assoc();
        if ($row) return $row[0];
        else return false;
    }

    public function getResultRow($sql)
    {
        if ($result = $this->query($sql)) {
            $row = $result->fetch_assoc();
            return $row;
        } else {
            return false;
        }
    }

    public function getResultArray($sql)
    {
        $result = $this->query($sql);
        $list = array();
        while ($row = $result->fetch_assoc()) {
            $list[] = $row;
        }
        return $list;
    }

    public function getCol($sql)
    {
        $result = $this->query($sql);
        $list = array();
        while ($row = $result->fetch_assoc()) {
            $list[] = $row[0];
        }
        return $list;
    }

    public function getInsertId()
    {
        return mysqli_insert_id($this->conn);
    }
}
