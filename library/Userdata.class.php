<?php
class UserData
{
    protected $user_id = '';
    protected $user_name = '';
    protected $name = '';
    protected $user_type = '';

    public function __construct()
    {
    }

    public function setUserData($string, $value)
    {
        $this->$string = $value;
        $_SESSION[$string] = $value;
    }

    public function getUserData($string)
    {
        return $_SESSION[$string];
    }

    public function deleteUserData($string)
    {
        $this->$string = '';
        $_SESSION[$string] = '';
    }
}
